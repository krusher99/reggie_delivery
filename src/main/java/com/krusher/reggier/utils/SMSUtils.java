package com.krusher.reggier.utils;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.dysmsapi20170525.models.*;

/**
 * 短信发送工具类
 */
public class SMSUtils {

	/**
	 * 发送短信
	 * @param phoneNumbers 手机号
	 * @param param 验证码
	 * @throws Exception
	 */
    public static void sendMessage(String phoneNumbers, String param) throws Exception {

        Client client1 = createClient("LTAI5tST53fn9rtzqxbJKBWX", "gCTJgonQElR42VodlvOP4qgGB9YkOA");
        SendSmsRequest sendSmsRequest1 = new SendSmsRequest();
        sendSmsRequest1.setSignName("阿里云短信测试");
        sendSmsRequest1.setTemplateCode("SMS_154950909");
        sendSmsRequest1.setPhoneNumbers(phoneNumbers);
        sendSmsRequest1.setTemplateParam("{\"code\":\"" + param + "\"}");

        client1.sendSms(sendSmsRequest1);


    }

    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

}
