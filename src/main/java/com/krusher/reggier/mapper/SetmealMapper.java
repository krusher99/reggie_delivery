package com.krusher.reggier.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.krusher.reggier.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
