package com.krusher.reggier.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.krusher.reggier.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
