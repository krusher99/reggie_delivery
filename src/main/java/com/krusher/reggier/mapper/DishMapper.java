package com.krusher.reggier.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.krusher.reggier.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}
