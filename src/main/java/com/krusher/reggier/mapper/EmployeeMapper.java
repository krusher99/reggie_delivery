package com.krusher.reggier.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.krusher.reggier.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
