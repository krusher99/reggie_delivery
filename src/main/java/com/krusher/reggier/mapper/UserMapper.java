package com.krusher.reggier.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.krusher.reggier.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
