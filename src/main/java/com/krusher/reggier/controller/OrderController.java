package com.krusher.reggier.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.krusher.reggier.common.BaseContext;
import com.krusher.reggier.common.R;
import com.krusher.reggier.entity.Orders;
import com.krusher.reggier.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 用户下单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        log.info("订单数据: {}",orders);

        orderService.submit(orders);

        return R.success("下单成功");
    }

    /**
     * 查看订单
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page> userPage(int page,int pageSize){

        log.info("page = {},pageSize = {}", page, pageSize);

        //构造分页构造器
        Page pageInfo = new Page(page, pageSize);

        //构造条件构造器
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper();

        Long userId = BaseContext.getCurrentId();

        queryWrapper.eq(Orders::getUserId,userId);

        queryWrapper.orderByDesc(Orders::getOrderTime);

        orderService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);
    }

    /**
     * 根据条件查询
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize,String number,String beginTime,String endTime){

        log.info("page = {},pageSize = {},number = {},beginTime = {},endTime = {}", page, pageSize,number,beginTime,endTime);

        //构造分页构造器
        Page pageInfo = new Page(page, pageSize);

        //构造条件构造器
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper();

        queryWrapper.orderByDesc(Orders::getOrderTime);

        if (number != null){
            queryWrapper.eq(Orders::getNumber,number);
        }if (beginTime != null){
            queryWrapper.ge(Orders::getOrderTime,beginTime);
        }if (endTime != null){
            queryWrapper.le(Orders::getOrderTime,endTime);
        }

        orderService.page(pageInfo,queryWrapper);

        return R.success(pageInfo);
    }

    @PutMapping
    public R<String> send(@RequestBody Orders orders){
        log.info("订单信息: {}",orders);

        LambdaUpdateWrapper<Orders> updateWrapper = new LambdaUpdateWrapper<>();

        updateWrapper.eq(Orders::getId,orders.getId())
                .set(Orders::getStatus,orders.getStatus());

        orderService.update(updateWrapper);

        return R.success("订单已送出");
    }

}
