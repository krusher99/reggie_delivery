package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.dto.DishDto;
import com.krusher.reggier.entity.Dish;

public interface DishService extends IService<Dish> {

    //新增菜品，同时插入菜品对应的口味数据，需要操作两张表：dish、dish_flavor
    public void saveWithFlavor(DishDto dishDto);

    //根据id来查询菜品信息和对应的口味信息
    public DishDto getByIdWithFlavor(Long id);

    //更新菜品信息，同时更新对应的口味信息
    public void updateWithFlavor(DishDto dishDto);

    //更新菜品停售和启售状态
    public void activeOrNot(int status, Long[] ids);

    //删除菜品同时停售包含该菜品的套餐
    public void removeWithSetmealAndFlavor(Long[] ids);
}
