package com.krusher.reggier.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.krusher.reggier.entity.AddressBook;
import com.krusher.reggier.mapper.AddressBookMapper;
import com.krusher.reggier.service.AddressBookService;
import org.springframework.stereotype.Service;

@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
}
