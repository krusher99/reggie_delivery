package com.krusher.reggier.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.krusher.reggier.entity.SetmealDish;
import com.krusher.reggier.mapper.SetmealDishMapper;
import com.krusher.reggier.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
