package com.krusher.reggier.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.krusher.reggier.entity.User;
import com.krusher.reggier.mapper.UserMapper;
import com.krusher.reggier.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
