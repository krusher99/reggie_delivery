package com.krusher.reggier.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.krusher.reggier.entity.OrderDetail;
import com.krusher.reggier.mapper.OrderDetailMapper;
import com.krusher.reggier.service.OrderDetailService;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {
}
