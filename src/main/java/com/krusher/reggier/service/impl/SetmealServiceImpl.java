package com.krusher.reggier.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.krusher.reggier.common.CustomException;
import com.krusher.reggier.dto.SetmealDto;
import com.krusher.reggier.entity.Category;
import com.krusher.reggier.entity.Setmeal;
import com.krusher.reggier.entity.SetmealDish;
import com.krusher.reggier.mapper.SetmealMapper;
import com.krusher.reggier.service.CategoryService;
import com.krusher.reggier.service.SetmealDishService;
import com.krusher.reggier.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private CategoryService categoryService;

    /**
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     *
     * @param setmealDto
     */
    @Override
    public void saveWithDish(SetmealDto setmealDto) {

        //保存套餐的基本信息，操作setmeal，执行insert操作
        this.save(setmealDto);

        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes = setmealDishes.stream().map((item) -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //保存套餐和菜品的关联信息，操作setmeal_dish，执行insert操作
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     *
     * @param ids
     */
    @Override
    public void removeWithDish(Long[] ids) {

        //查询套餐状态，确定是否可以删除
        for (Long id : ids) {
            LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
            LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();
            queryWrapper.eq(Setmeal::getId, id);
            queryWrapper.eq(Setmeal::getStatus, 0);

            int count = this.count(queryWrapper);

            //如果不能删除，抛出一个业务异常
            if (count == 0){
                throw new CustomException("套餐售卖中，不能删除");
            }

            //如果可以删除，先删除套餐表中的数据--setmeal
            updateWrapper.set(Setmeal::getIsDeleted, 1)
                    .eq(Setmeal::getId, id)
                    .eq(Setmeal::getStatus, 0);
            this.update(updateWrapper);
        }
    }

    /**
     * 修改套餐的状态
     *
     * @param ids
     * @param status
     */
    @Override
    public void setMealActive(Long[] ids, int status) {

        for (Long id : ids) {
            LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(Setmeal::getId, id).set(Setmeal::getStatus, status);
            this.update(updateWrapper);
        }
    }

    /**
     * 根据id获得套餐详细信息
     *
     * @param id
     * @return
     */
    @Override
    public SetmealDto getByIdWithDish(Long id) {

        //查询套餐基本信息，->setmeal
        Setmeal setmeal = this.getById(id);

        SetmealDto setmealDto = new SetmealDto();
        BeanUtils.copyProperties(setmeal, setmealDto);

        //根据菜品分类id查询该分类的名字
        Long categoryId = setmeal.getCategoryId();
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryLambdaQueryWrapper.eq(Category::getId, categoryId);
        Category category = categoryService.getOne(categoryLambdaQueryWrapper);
        //设置分类名称
        setmealDto.setCategoryName(category.getName());

        //查询当前套餐的基本信息，包含菜品
        LambdaQueryWrapper<SetmealDish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> setmealDishList = setmealDishService.list(dishLambdaQueryWrapper);

        setmealDto.setSetmealDishes(setmealDishList);

        return setmealDto;
    }
}
