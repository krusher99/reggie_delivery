package com.krusher.reggier.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.krusher.reggier.entity.Employee;
import com.krusher.reggier.mapper.EmployeeMapper;
import com.krusher.reggier.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
}
