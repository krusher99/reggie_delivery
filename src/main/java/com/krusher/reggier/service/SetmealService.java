package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.dto.SetmealDto;
import com.krusher.reggier.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {

    /**
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     * @param setmealDto
     */
    public void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    public void removeWithDish(Long[] ids);

    /**
     * 修改套餐的状态
     * @param ids
     * @param status
     */
    public void setMealActive(Long[] ids, int status);

    /**
     * 根据id获得套餐详细信息
     * @param id
     * @return
     */
    public SetmealDto getByIdWithDish(Long id);
}
