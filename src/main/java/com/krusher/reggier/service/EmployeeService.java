package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.Employee;

public interface EmployeeService extends IService<Employee> {

}
