package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
