package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {
}
