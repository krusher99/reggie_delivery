package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
