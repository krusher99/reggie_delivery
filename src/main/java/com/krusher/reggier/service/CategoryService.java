package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.Category;

public interface CategoryService extends IService<Category> {

    public void remove(Long id);

}
