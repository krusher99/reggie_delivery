package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
