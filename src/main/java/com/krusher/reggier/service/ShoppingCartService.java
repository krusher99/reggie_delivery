package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
}
