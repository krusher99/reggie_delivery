package com.krusher.reggier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.krusher.reggier.entity.Orders;
import org.springframework.core.annotation.Order;

public interface OrderService extends IService<Orders> {

    /**
     * 用户西单
     * @param orders
     */
    public void submit(Orders orders);

}
